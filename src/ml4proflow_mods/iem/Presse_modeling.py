# imports
from csv import QUOTE_NONE
from typing import Tuple
import numpy as np
import scipy.signal as sci_sig
import scipy.integrate as sci_int
import matplotlib.pyplot as plt
import pandas as pd
import os
import json
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense
import tensorflow as tf

# Halbpressenmodell
class Halbpresse():
    '''Modell einer Halbpresse mit Platine, Werkzeug und Kühlwasserkanal'''
    def __init__(self, alpha=4000):
        self.alpha = alpha # W/m2K
        self.A = 1 # m^2
        self.mPl_cPl = 6500 # m = 10 kg; c_pPl = 650 J/kgK
        self.mWZ_cWZ = 500000 # m_WZ = 1000kg; c_WZ = 500 J/kgK
        self.c_pKW = 4200 # J/kgK
    
    def setup_StateSpace(self):
        self.A = np.array([
            [-self.alpha*self.A / (self.mPl_cPl), self.alpha*self.A / (self.mPl_cPl)],
            [self.alpha*self.A / (self.mWZ_cWZ), -self.alpha*self.A / (self.mWZ_cWZ)]
            ])

        self.B = np.array([
            [0],
            [-self.c_pKW/(self.mWZ_cWZ)]
            ])

        self.C = np.array([
            [1,0],
            [0,1]
            ])

        self.D = np.array([
            [0],
            [0]
        ])

        self.sys = sci_sig.StateSpace(self.A, self.B, self.C, self.D)

    def test_simulation(self, t_span=10):
        t = np.arange(0, t_span, 0.01) # Zeitvektor für Simulation
        m_KW_dot = 10 # kg/s Massenstrom Kühlwasser
        delta_t_KW = 5 # K Temperaturdelta Kühlwasserausgang zu -eingang
        u = np.ones_like(t) * (m_KW_dot*delta_t_KW)
        u = np.linspace(0, delta_t_KW, len(t)) * m_KW_dot
        Xinit = np.array([[900+273.15, 20+273.15]])

        tout,yout,xout = sci_sig.lsim(self.sys,u,t,Xinit)

        plt.plot(tout,yout-273.15)
        plt.title('Test Simulation')
        plt.legend(['T_Platine','T_Werkzeug'])
        plt.grid()
        plt.xlabel('Time [s]')
        plt.ylabel('Temperature [°C]')
        plt.show()

# Vollpressenmodell
class Vollpresse():
    '''Modell einer Vollpresse mit Platine, Werkzeug oben/unten und Kühlwasserkanal oben/unten'''
    def __init__(self, alpha_oben=4, alpha_unten=4, mPl=10, mWZoben=1, mWZunten=1):
        self.alpha_oben = alpha_oben*1000 # W/m2K
        self.alpha_unten = alpha_unten*1000 # W/m2K
        self.Area = 1 # m^2
        self.mPl = mPl # kg
        self.cPl = 650 # 650 J/kgK
        self.mWZoben = mWZoben*1000 # kg
        self.mWZunten = mWZunten*1000 # kg
        self.cWZ = 500 # 500 J/kgK
        self.cKW = 4200 # J/kgK



    def setup_StateSpace(self):
        self.A = np.array([
            [-(self.alpha_oben+self.alpha_unten)*self.Area / (self.mPl*self.cPl), self.alpha_oben*self.Area / (self.mPl*self.cPl), self.alpha_unten*self.Area / (self.mPl*self.cPl)],
            [self.alpha_oben*self.Area / (self.mWZoben*self.cWZ), -self.alpha_oben*self.Area / (self.mWZoben*self.cWZ), 0],
            [self.alpha_unten*self.Area / (self.mWZunten*self.cWZ), 0, -self.alpha_unten*self.Area / (self.mWZunten*self.cWZ)]
            ])

        self.B = np.array([
            [0, 0],
            [-self.cKW/(self.mWZoben*self.cWZ), 0], 
            [0, -self.cKW/(self.mWZunten*self.cWZ)]
            ])

        self.C = np.array([
            [1,0,0],
            [0,1,0],
            [0,0,1]
            ])

        self.D = np.array([
            [0,0],
            [0,0],
            [0,0]
        ])

        self.sys = sci_sig.StateSpace(self.A, self.B, self.C, self.D)



    def set_stroke_df(self, full_df:pd.DataFrame, identifier, time_range=(10,10), product= 'Fußraum_oben'):
        '''
        Slice and prepare stroke_df from given (batch,batch_index) identifier and time_range
        identifier: (batch, batch_index) | timestamp
        '''
        resolution = '100ms'

        # get timestamp of stroke
        if type(identifier)==tuple:
            batch, batch_index = identifier
            self.ts = full_df[(full_df['batch']==batch) & (full_df['batch_index']==batch_index)].index
            waittime = full_df.loc[self.ts,'waittime'].values
            assert len(self.ts)==1, f'Could not find stroke {batch_index} of batch {batch}'
            self.ts = pd.Timestamp(self.ts[0]) # get timestamp from index
        elif type(identifier)==pd.Timestamp:
            self.ts = identifier
        else:
            print(f'Could not interpret identifier of type {type(identifier)}')
        self.ts = self.ts.round(resolution)
        
        # select signal columns and assert product
        assert product in ['Fußraum_oben', 'Fußraum_unten'], "Parameter product needs to have one of the following values: ['Fußraum_oben', 'Fußraum_unten']" # assert product
        self.product = product
        sigs = ['Werkzeugkuehlung Vorlauf Temp_[°C]',
            'vorlauf_correction',
            f'Werkzeugkuehlung {self.product} Oberwerkz Temp_[°C]',
            f'Werkzeugkuehlung {self.product} Oberwerkz Durchfl_[L/min]',
            f'Werkzeugkuehlung {self.product} Unterwerkz Temp_[°C]',
            f'Werkzeugkuehlung {self.product} Unterwerkz Durchfl_[L/min]'
            ]
        
        # build stroke_df
        full_df = full_df.interpolate(method='time')
        self.stroke_df = full_df.loc[self.ts-pd.Timedelta(f'{time_range[0]}s') : self.ts+pd.Timedelta(f'{time_range[1]}s'), sigs]
        self.stroke_df = self.stroke_df.resample(resolution).mean()
        self.stroke_df = self.stroke_df.interpolate(method='time').round(2)

        # convert flowrate from L/min to L/s and assert active state of channel
        self.stroke_df[f'Werkzeugkuehlung {self.product} Oberwerkz Durchfl_[L/min]'] = self.stroke_df[f'Werkzeugkuehlung {self.product} Oberwerkz Durchfl_[L/min]'] / 60
        self.stroke_df[f'Werkzeugkuehlung {self.product} Unterwerkz Durchfl_[L/min]'] = self.stroke_df[f'Werkzeugkuehlung {self.product} Unterwerkz Durchfl_[L/min]'] / 60
        rename_flow = {
            f'Werkzeugkuehlung {self.product} Oberwerkz Durchfl_[L/min]':f'Werkzeugkuehlung {self.product} Oberwerkz Durchfl_[L/s]',
            f'Werkzeugkuehlung {self.product} Unterwerkz Durchfl_[L/min]':f'Werkzeugkuehlung {self.product} Unterwerkz Durchfl_[L/s]'
            }
        self.stroke_df.rename(columns=rename_flow, inplace=True)

        flow_rate_oben = self.stroke_df[f'Werkzeugkuehlung {self.product} Oberwerkz Durchfl_[L/s]'].mean()
        flow_rate_unten = self.stroke_df[f'Werkzeugkuehlung {self.product} Unterwerkz Durchfl_[L/s]'].mean()
        assert 1 < flow_rate_oben < 10, f'flow_rate_oben is not within common range -> {flow_rate_oben}'
        assert 1 < flow_rate_unten < 10, f'flow_rate_unten is not within common range -> {flow_rate_unten}'

        # integrate vorlauf correction
        self.stroke_df.loc[:,'Werkzeugkuehlung Vorlauf Temp_[°C]'] = self.stroke_df.loc[:,'Werkzeugkuehlung Vorlauf Temp_[°C]'] - self.stroke_df.loc[:,'vorlauf_correction'].mean()
        self.stroke_df.drop(['vorlauf_correction'], axis=1, inplace=True)



    def calc_total_energy_flow(self)->Tuple:
        '''Calculate total energy flow to cooling water
        Returns: (Q_soll_oben, Q_soll_unten)'''
        # oben
        ts_min_pre_oben = self.stroke_df.loc[:self.ts, f'Werkzeugkuehlung {self.product} Oberwerkz Temp_[°C]'].argmin()
        ts_min_pre_oben = self.stroke_df.loc[:self.ts, f'Werkzeugkuehlung {self.product} Oberwerkz Temp_[°C]'].index[ts_min_pre_oben]

        ts_min_past_oben = self.stroke_df.loc[self.ts : , f'Werkzeugkuehlung {self.product} Oberwerkz Temp_[°C]'].argmin()
        ts_min_past_oben = self.stroke_df.loc[self.ts : , f'Werkzeugkuehlung {self.product} Oberwerkz Temp_[°C]'].index[ts_min_past_oben]

        delta_T_oben = self.stroke_df.loc[ts_min_pre_oben : ts_min_past_oben, f'Werkzeugkuehlung {self.product} Oberwerkz Temp_[°C]'] - self.stroke_df.loc[ts_min_pre_oben : ts_min_past_oben, 'Werkzeugkuehlung Vorlauf Temp_[°C]']
        Q_dot_oben = delta_T_oben * self.stroke_df.loc[ts_min_pre_oben : ts_min_past_oben, f'Werkzeugkuehlung {self.product} Oberwerkz Durchfl_[L/s]'] * self.cKW

        Q_soll_oben = sci_int.simpson(Q_dot_oben.to_numpy(), dx=0.1)

        # unten
        ts_min_pre_unten = self.stroke_df.loc[:self.ts, f'Werkzeugkuehlung {self.product} Unterwerkz Temp_[°C]'].argmin()
        ts_min_pre_unten = self.stroke_df.loc[:self.ts, f'Werkzeugkuehlung {self.product} Unterwerkz Temp_[°C]'].index[ts_min_pre_unten]

        ts_min_past_unten = self.stroke_df.loc[self.ts : , f'Werkzeugkuehlung {self.product} Unterwerkz Temp_[°C]'].argmin()
        ts_min_past_unten = self.stroke_df.loc[self.ts : , f'Werkzeugkuehlung {self.product} Unterwerkz Temp_[°C]'].index[ts_min_past_unten]

        delta_T_unten = self.stroke_df.loc[ts_min_pre_unten : ts_min_past_unten, f'Werkzeugkuehlung {self.product} Unterwerkz Temp_[°C]'] - self.stroke_df.loc[ts_min_pre_unten : ts_min_past_unten, 'Werkzeugkuehlung Vorlauf Temp_[°C]']
        Q_dot_unten = delta_T_unten * self.stroke_df.loc[ts_min_pre_unten : ts_min_past_unten, f'Werkzeugkuehlung {self.product} Unterwerkz Durchfl_[L/s]'] * self.cKW

        Q_soll_unten = sci_int.simpson(Q_dot_unten.to_numpy(), dx=0.1)
        return (Q_soll_oben, Q_soll_unten)



    def simulate_stroke(self, plot=True):
        # get signal features
        # Oberwerkzeug
        t_min_oben = self.stroke_df.loc[self.ts-pd.Timedelta('10s') : self.ts, f'Werkzeugkuehlung {self.product} Oberwerkz Temp_[°C]'].min()
        ts_t_min_oben = self.stroke_df.loc[self.ts-pd.Timedelta('10s') : self.ts, f'Werkzeugkuehlung {self.product} Oberwerkz Temp_[°C]'].argmin()
        ts_t_min_oben = self.stroke_df.loc[self.ts-pd.Timedelta('10s') : self.ts,f'Werkzeugkuehlung {self.product} Oberwerkz Temp_[°C]'].index[ts_t_min_oben]

        t_max_oben = self.stroke_df.loc[self.ts : self.ts+pd.Timedelta('10s'), f'Werkzeugkuehlung {self.product} Oberwerkz Temp_[°C]'].max()
        ts_t_max_oben = self.stroke_df.loc[self.ts : self.ts+pd.Timedelta('10s'), f'Werkzeugkuehlung {self.product} Oberwerkz Temp_[°C]'].argmax()
        ts_t_max_oben = self.stroke_df.loc[self.ts : self.ts+pd.Timedelta('10s'),f'Werkzeugkuehlung {self.product} Oberwerkz Temp_[°C]'].index[ts_t_max_oben]
        # shift Temperature signal due to Totzeit
        duration_heat_oben = ts_t_max_oben-ts_t_min_oben
        #print('Dauer oben:', duration_heat_oben)
        if duration_heat_oben.total_seconds() < 10:
            totzeit_oben = (self.ts-ts_t_min_oben)-pd.Timedelta('10s')
        else:
            totzeit_oben = self.ts-ts_t_max_oben
        self.stroke_df[f'Werkzeugkuehlung {self.product} Oberwerkz Temp_[°C]'] = self.stroke_df[f'Werkzeugkuehlung {self.product} Oberwerkz Temp_[°C]'].shift(freq=totzeit_oben)

        # Unterwerkzeug
        t_min_unten = self.stroke_df.loc[self.ts-pd.Timedelta('10s') : self.ts, f'Werkzeugkuehlung {self.product} Unterwerkz Temp_[°C]'].min()
        ts_t_min_unten = self.stroke_df.loc[self.ts-pd.Timedelta('10s') : self.ts, f'Werkzeugkuehlung {self.product} Unterwerkz Temp_[°C]'].argmin()
        ts_t_min_unten = self.stroke_df.loc[self.ts-pd.Timedelta('10s') : self.ts,f'Werkzeugkuehlung {self.product} Unterwerkz Temp_[°C]'].index[ts_t_min_unten]

        t_max_unten = self.stroke_df.loc[self.ts : self.ts+pd.Timedelta('10s'), f'Werkzeugkuehlung {self.product} Unterwerkz Temp_[°C]'].max()
        ts_t_max_unten = self.stroke_df.loc[self.ts : self.ts+pd.Timedelta('10s'), f'Werkzeugkuehlung {self.product} Unterwerkz Temp_[°C]'].argmax()
        ts_t_max_unten = self.stroke_df.loc[self.ts : self.ts+pd.Timedelta('10s'),f'Werkzeugkuehlung {self.product} Unterwerkz Temp_[°C]'].index[ts_t_max_unten]
        # shift Temperature signal due to Totzeit
        duration_heat_unten = ts_t_max_unten-ts_t_min_unten
        if duration_heat_unten.total_seconds() < 10:
            totzeit_unten = (self.ts-ts_t_min_unten)-pd.Timedelta('10s')
        else:
            totzeit_unten = self.ts-ts_t_max_unten
        self.stroke_df[f'Werkzeugkuehlung {self.product} Unterwerkz Temp_[°C]'] = self.stroke_df[f'Werkzeugkuehlung {self.product} Unterwerkz Temp_[°C]'].shift(freq=totzeit_unten)
        
        # simulate model
        self.setup_StateSpace()
        t = np.arange(0, 10.1, 0.1) # Zeitvektor für Simulation
        u_oben = ((self.stroke_df.loc[self.ts-pd.Timedelta('10s'): self.ts,f'Werkzeugkuehlung {self.product} Oberwerkz Temp_[°C]'] - 
                    self.stroke_df.loc[self.ts-pd.Timedelta('10s'): self.ts,'Werkzeugkuehlung Vorlauf Temp_[°C]'] ) *
                    self.stroke_df.loc[self.ts-pd.Timedelta('10s'): self.ts, f'Werkzeugkuehlung {self.product} Oberwerkz Durchfl_[L/s]']).to_numpy()
        u_unten = ((self.stroke_df.loc[self.ts-pd.Timedelta('10s'): self.ts, f'Werkzeugkuehlung {self.product} Unterwerkz Temp_[°C]'] -
                    self.stroke_df.loc[self.ts-pd.Timedelta('10s'): self.ts, 'Werkzeugkuehlung Vorlauf Temp_[°C]'] ) *
                    self.stroke_df.loc[self.ts-pd.Timedelta('10s'): self.ts, f'Werkzeugkuehlung {self.product} Unterwerkz Durchfl_[L/s]']).to_numpy()
        u = np.c_[u_oben,u_unten]
        Xinit = np.array([[900, t_min_oben, t_min_unten]])
        tout,yout,xout = sci_sig.lsim(self.sys, u, t, Xinit)
        
        # Add simulation result to self.stroke_df
        self.stroke_df.loc[self.ts-pd.Timedelta('10s'): self.ts, 'T_Platine_[°C]'] = yout[:,0]
        self.stroke_df.loc[self.ts-pd.Timedelta('10s'): self.ts, 'T_Werkzeug_oben_[°C]'] = yout[:,1]
        self.stroke_df.loc[self.ts-pd.Timedelta('10s'): self.ts, 'T_Werkzeug_unten_[°C]'] = yout[:,2]
        
        # Plot signals in figure
        if plot:
            fig = self.stroke_df.plot(title=f'Stroke Simulation (ts={self.ts})')
            fig.show()

    

    def calc_blank_energy_flow(self)->Tuple:
        Q_dot_oben = self.alpha_oben * self.Area * (self.stroke_df.loc[self.ts-pd.Timedelta('10s'):self.ts, 'T_Platine_[°C]'] - self.stroke_df.loc[self.ts-pd.Timedelta('10s'):self.ts, 'T_Werkzeug_oben_[°C]'])
        Q_dot_unten = self.alpha_unten * self.Area * (self.stroke_df.loc[self.ts-pd.Timedelta('10s'):self.ts, 'T_Platine_[°C]'] - self.stroke_df.loc[self.ts-pd.Timedelta('10s'):self.ts, 'T_Werkzeug_unten_[°C]'])

        Q_ist_oben = sci_int.simpson(Q_dot_oben.to_numpy(), dx=0.1)
        Q_ist_unten = sci_int.simpson(Q_dot_unten.to_numpy(), dx=0.1)
        return (Q_ist_oben, Q_ist_unten)


    def test_simulation(self):
        t = np.arange(0, 10, 0.01) # Zeitvektor für Simulation
        m_KW_oben_dot = 10 # kg/s Massenstrom Kühlwasser oben
        m_KW_unten_dot = 10 # kg/s Massenstrom Kühlwasser unten

        delta_t_KW = 5 # K Temperaturdelta Kühlwasserausgang zu -eingang

        u_oben = np.linspace(0, delta_t_KW, len(t)) * m_KW_oben_dot
        u_unten = np.linspace(0, delta_t_KW, len(t)) * m_KW_unten_dot
        u = np.c_[u_oben, u_unten]

        Xinit = np.array([[900+273.15, 20+273.15, 20+273.15]])

        tout,yout,xout = sci_sig.lsim(self.sys, u, t, Xinit)

        plt.plot(tout,yout-273.15)
        plt.title('Test Simulation')
        plt.legend(['T_Platine', 'T_Werkzeug_oben', 'T_Werkzeug_unten'])
        plt.grid()
        plt.ylim(20,40)
        plt.show()


# read data function
data_path = 'c:\\Users\\lzc\\OneDrive - Fraunhofer\\ML4Pro2\\ML4Pro2_Benteler_IEM\\Git_Repo\\JupyDocker\\Data'
def read_df(filename:str, data_path=data_path)->pd.DataFrame:
    '''
    Please change data path if needed!
    Default:
    data_path = 'c:\\Users\\lzc\\OneDrive - Fraunhofer\\ML4Pro2\\ML4Pro2_Benteler_IEM\\Git_Repo\\JupyDocker\\Data'
    '''
    df = pd.read_csv(os.path.join(data_path, filename))
    df['time'] = pd.to_datetime(df['time'])
    df.set_index('time', inplace=True)

    # Umwandlung von Signalnamen in Klarnamen aus signaldictionary.json
    signaldictionary = json.load(open(os.path.join(data_path,'signaldictionary.json')))
    df.rename(columns=signaldictionary, inplace=True)
    return df

# set batch features function
def set_batch_features(df:pd.DataFrame, maximum_waittime=120, log=False) -> pd.DataFrame:
    '''
    set value for 'waittime'
    set value for 'batch'
    set value for 'batch_index'
    set value for 'vorlauf_correction'
    '''
    # Finding peaks in 'Presse Ist-Zuhaltezeit_[s]' for stroke definition
    peak_idx,_ = sci_sig.find_peaks(df['Presse Ist-Zuhaltezeit_[s]'].dropna()) # get local peak indexes
    peak_df_idx = df['Presse Ist-Zuhaltezeit_[s]'].dropna().iloc[peak_idx].index # get global peak indexes

    # Calculate waittime
    df.loc[peak_df_idx[1:],'waittime'] = peak_df_idx.to_series().diff()[1:] # calculate waittime except for first row
    df.loc[peak_df_idx[1:],'waittime'] = [time_delta.total_seconds() for time_delta in df.loc[peak_df_idx[1:],'waittime']] # convert waittime to total_seconds
    df.loc[peak_df_idx,'waittime'].fillna(0, inplace=True) # set first waittime to zero if nan

    # Sort products into batches
    df.loc[peak_df_idx,'batch'] = np.where(df.loc[peak_df_idx,'waittime'] > maximum_waittime,1,0) # write '1' in each line where waittime > maximum_waittime, else write 0
    if df['batch'].max() ==  1: # df has no batch sorting yet, begin with 1
        batch_max_idx = df['batch'].idxmax()  # get first idxmax from batch column
        if log: print('Batch Initialization')
    else:
        batch_max_idx = df['batch'][::-1].idxmax() # get last idxmax from batch column
        if log: print('Append Batches after',df['batch'][::-1].max())
        #display(df.loc[batch_max_idx:,'batch'].dropna())

    df.loc[batch_max_idx:,'batch'] = df.loc[batch_max_idx:,'batch'].cumsum() # Assigning batch numbers with cumsum methode beginning on idxmax index position; np.nan lines are overlooked

    # add 'batch_size' column
    df.loc[peak_df_idx,'batch_size'] = df.loc[peak_df_idx,:].groupby('batch')['batch'].transform('count')
    
    # batch_index calculation
    duplicates = df.loc[peak_df_idx,['batch','batch_size']].duplicated(keep='first') # mark first row of each batch as False, all others True
    df.loc[peak_df_idx,'batch_index'] = np.where(duplicates,1,0) # write '1' in each line where True, if False write 0
    df.loc[peak_df_idx,'batch_index'] = df.loc[peak_df_idx,:].groupby('batch')['batch_index'].transform('cumsum') # group by 'batch' column and transform 'batch_index' column with cumsum method
    
    # Vorlauf Korrekturfaktor
    # ts_tmin_unten
    for ts in df[df['batch_index']==0].index:
        batch = df.loc[ts,'batch'] # get current batch
        T_min_unten = df.loc[ts-pd.Timedelta('20s') : ts, 'Werkzeugkuehlung Fußraum_unten Unterwerkz Temp_[°C]'].min()
        T_vorlauf = df.loc[ts-pd.Timedelta('20s') : ts, 'Werkzeugkuehlung Vorlauf Temp_[°C]'].mean()
        vorlauf_correction = T_vorlauf - T_min_unten # calculate vorlauf_correction
        mask = (df['batch']==batch)
        df.loc[mask,'vorlauf_correction'] = vorlauf_correction # write vorlauf_correction

    return df



# set cooling water features
def set_cooling_water_features(df, warnings=False):
    '''
    set stroke value for 'Werkzeugkuehlung Vorlauf Temp_[°C]' by interpolation
    set stroke values of cooling water outflow Min, Max, Delta
    '''
    sigs_cw = ['Werkzeugkuehlung Fußraum_oben Oberwerkz Temp_[°C]', 
        'Werkzeugkuehlung Fußraum_unten Oberwerkz Temp_[°C]',
        'Werkzeugkuehlung Fußraum_oben Unterwerkz Temp_[°C]',
        'Werkzeugkuehlung Fußraum_unten Unterwerkz Temp_[°C]']

    sigs_flow = ['Werkzeugkuehlung Fußraum_oben Oberwerkz Durchfl_[L/min]',
       'Werkzeugkuehlung Fußraum_unten Oberwerkz Durchfl_[L/min]',
       'Werkzeugkuehlung Fußraum_oben Unterwerkz Durchfl_[L/min]',
       'Werkzeugkuehlung Fußraum_unten Unterwerkz Durchfl_[L/min]']

    sig_vor = 'Werkzeugkuehlung Vorlauf Temp_[°C]'

    # interpolate 'Werkzeugkuehlung Vorlauf Temp_[°C]' signal
    df.loc[:, 'Werkzeugkuehlung Vorlauf Temp_[°C]'].interpolate(method='time', inplace=True)
    
    # get start ts for cooling water feature extraction
    ts_start_cw_feature = df[sigs_cw].dropna(how='all').index[0]

    # Min, Max, Mean, Delta definition of cooling water signals (including some warning prints)
    ts_batch = df.loc[ts_start_cw_feature:, 'batch'].dropna()
    for ts in ts_batch.index:
        for sig in sigs_cw:
            min_temp = df.loc[ts-pd.Timedelta('10s') : ts, sig].min() # -10s -> ts "Start-Temperatur des Strokes"
            max_temp = df.loc[ts-pd.Timedelta('2s') : ts + pd.Timedelta('9s'), sig].max() # -2s -> +9s
            df.at[ts, f'{sig}_Min'] = min_temp
            df.at[ts, f'{sig}_Max'] = max_temp

        for sig in sigs_flow:
            max_flow = df.loc[ts-pd.Timedelta('10s') : ts + pd.Timedelta('2s'), sig].max() # -10s -> +2s
            min_flow = df.loc[ts-pd.Timedelta('10s') : ts + pd.Timedelta('2s'), sig].min() # -10s -> +2s
            flow_range = round(max_flow - min_flow, 2)
            
            mean_flow = df.loc[ts-pd.Timedelta('10s') : ts + pd.Timedelta('2s'), sig].mean() # -10s -> +2s
            df.at[ts, f'{sig}_Mean'] = mean_flow
            # Raise Warning when Std of flow too high
            std_flow = df.loc[ts-pd.Timedelta('10s') : ts + pd.Timedelta('2s'), sig].std() # -10s -> +2s
            if (std_flow > 10) & (warnings==True): 
                print(f'Std of {sig}  at {ts.ceil(freq="s")} is too high!\tStd: {round(std_flow,2)} [L/min], Mean: {round(mean_flow,2)} [L/min]')
                df.loc[ts-pd.Timedelta('10s') : ts + pd.Timedelta('2s'), sig].dropna().plot(figsize=(15,5), label=sig)
                plt.legend()
        

        # vorlauf features
        mean_vor = df.loc[ts-pd.Timedelta('10s') : ts + pd.Timedelta('2s'), sig_vor].mean() # -10s -> +2s
        df.at[ts, f'{sig_vor}_Mean'] = mean_vor
        # Raise Warning when Std too high
        std_vor = df.loc[ts-pd.Timedelta('10s') : ts + pd.Timedelta('2s'), sig_vor].std() # -10s -> +2s
        if (std_vor > 0.1) & (warnings==True):
            print(f'Std of {sig_vor}  at {ts.ceil(freq="s")} is too high!\tStd: {std_vor} [°C], Mean: {mean_vor} [°C]')
            df.loc[ts-pd.Timedelta('10s') : ts + pd.Timedelta('2s'), sig_vor].dropna().plot(figsize=(15,5), label=sig_vor)
            plt.legend()
        plt.show()
        
    for sig in sigs_cw:  
        df[f'{sig}_Delta'] = df[f'{sig}_Max']-df[f'{sig}_Min']

    return df


class temperature_prediction():
    '''Prediction of thermo image temperature using a neural network'''
    def __init__(self) -> None:
        self.test_size = 0.3
        self.train_features = [
            'Werkzeugkuehlung Vorlauf Temp_[°C]_Mean',
            'waittime',
            'Werkzeugkuehlung Fußraum_oben Oberwerkz Temp_[°C]_Min',
            'Werkzeugkuehlung Fußraum_oben Unterwerkz Temp_[°C]_Min',
            'Werkzeugkuehlung Fußraum_oben Oberwerkz Durchfl_[L/min]_Mean',
            'Werkzeugkuehlung Fußraum_oben Unterwerkz Durchfl_[L/min]_Mean',
            'Werkzeugkuehlung Fußraum_oben Oberwerkz Temp_[°C]_Delta',
            'Werkzeugkuehlung Fußraum_oben Unterwerkz Temp_[°C]_Delta'
            ]
        self.target_feature = 'img_fo_mean' # 'img_fu_mean'

    def prepare_training_data(self, df:pd.DataFrame)-> None:
        x = df.loc[:, self.train_features].values
        y = df.loc[:, self.target].values
        X_train, X_test, self.y_train, self.y_test = train_test_split(x, y, test_size=0.3, random_state=41)
        print('Train Shapes: ', X_train.shape, self.y_train.shape)
        print('Test Shapes: ', X_test.shape, self.y_test.shape)

        # Normalization
        sc = StandardScaler()

        self.X_train = sc.fit_transform(X_train)
        self.X_test = sc.transform(X_test)
        self.X_all = sc.transform(x)
        self.input_shape = X_train[0].shape

    def build_nn(self)-> None:
        # Neural Network Construction
        self.nn = Sequential([
            Dense(units=32, activation='relu', input_shape=self.input_shape, name='Input_Layer', kernel_initializer=tf.keras.initializers.HeUniform()), # input layer
            Dense(units=128, activation='relu', name='Hidden_1'),
            Dense(units=64, activation='relu', name='Hidden_2'),
            Dense(units=32, activation='relu', name='Hidden_3'),
            Dense(units=32, activation='relu', name='Hidden_4'),
            Dense(units=1, name='Output_Layer'), # output layer
        ], name='Simple_Dense')
        # custom loss function
        def custom_loss(y_true, y_pred):

            loss = tf.reduce_mean(tf.square(y_true-y_pred))

            return loss
        # NN Compile
        adam = tf.keras.optimizers.Adam(learning_rate=0.001)
        self.nn.compile(optimizer=adam, loss=custom_loss)

    def nn_training(self):
        # Training of model
        cb = tf.keras.callbacks.EarlyStopping(monitor='val_loss', min_delta=0.1, patience=100, restore_best_weights=True)
        history = self.nn.fit(self.X_train, self.y_train, batch_size=256, validation_data=(self.X_test, self.y_test), epochs=1000, callbacks = cb, verbose=2 )

    def nn_evaluation(self):
        # Evaluation of model
        results = self.nn.evaluate(self.X_test, self.y_test, batch_size=128)
        print("test loss", results)


    
