from pdb import pm
from ml4proflow.modules import (Module,DataFlowManager, SourceModule)

# import XXX # hier können Packages aus der setup.py importiert werden
from typing import Any, Dict
import pandas as pd

from .Presse_modeling import (set_batch_features, 
                              set_cooling_water_features,
                              temperature_prediction,
                              Vollpresse)



class Warmformen_IEM(Module):
    def __init__(self, dfm: DataFlowManager, config: Dict[str, Any]):
        Module.__init__(self, dfm, config)

        # Parameter
        self.config.setdefault('Module_batch', 3)
        self.config.setdefault('Module_Index of batch', 2)


    def on_new_data(self, name: str, sender: SourceModule,
                    data: pd.DataFrame) -> None:

        self.batch = self.config['Module_batch']
        self.batch_index = self.config['Module_Index of batch']
        #*** Funktionen aufrufen ***#
        df = set_batch_features(df) # setting batch features to incoming dataframe
        df = set_cooling_water_features(df)
        presse = Vollpresse()
        presse.set_stroke_df(df,(self.batch, self.batch_index))
        presse.simulate_stroke(plot=False)

        # Build and use neural net
        predictor = temperature_prediction()

        predictor.prepare_training_data(df)
        predictor.build_nn()
        predictor.nn_training()
        predictor.nn_evaluation()

        # Add simulation results to df
        mask = df['batch'==self.batch, 'batch_index'==self.batch_index]
        df.loc[mask,'T_Werkzeug_oben_[°C]'] = presse.stroke_df.loc[self.ts, 'T_Werkzeug_oben_[°C]']
        df.loc[mask,'T_Werkzeug_unten_[°C]'] = presse.stroke_df.loc[self.ts, 'T_Werkzeug_unten_[°C]']




        # letzte Aktion in der Funktion
        self._push_data(self.config['channels_push'][0], df) # hiermit werden die berechneten Daten in den Channel weitergegeben


