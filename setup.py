from setuptools import setup, find_packages

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

name = "ml4proflow-mods-iem"
version = "0.0.1"

cmdclass = {}

try:
    from sphinx.setup_command import BuildDoc
    cmdclass['build_sphinx'] = BuildDoc
except ImportError:
    print('WARNING: Sphinx not available, not building docs')

setup(
    name=name,
    version=version,
    author="Cederic Lenz",
    author_email="cederic.lenz@iem.fraunhofer.de",
    description="ml4proflow module for iem use case",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="todo",
    project_urls={
        "Main framework": "todo",
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    packages=find_packages(where="src"),
    package_dir={"": "src"},
    package_data={"ml4proflow": ["py.typed"]},
    data_files=[('ml4proflow/notebooks', ['notebooks/ml4proflow-mods-iem.ipynb'])],
    entry_points={
    },
    cmdclass=cmdclass,
    python_requires=">=3.6",  # todo
    install_requires=[
        "pandas",
        "ml4proflow",
        "numpy",
        "scipy",
        "tensorflow",
        "sklearn",
        "matplotlib",
        "ipython" # JupyterNotebook
    ],
    extras_require={
        "tests": ["pytest", 
                  "pandas-stubs",
                  "pytest-html",
                  "pytest-cov",
                  "flake8",
                  "mypy"],
        "docs": ["sphinx", "sphinx-rtd-theme", "recommonmark"],
    },
    command_options={
        'build_sphinx': {
            'project': ('setup.py', name),
            'version': ('setup.py', version),
            'release': ('setup.py', version),
            'source_dir': ('setup.py', 'docs/source/'),
            'build_dir': ('setup.py', 'docs/build/')
        }
    },
)
