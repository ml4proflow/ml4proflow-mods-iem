.. ml4proflow documentation master file, created by
   sphinx-quickstart on Fri Feb  4 12:55:51 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to ml4proflow\'s documentation!
=======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

.. include:: ../../README.md
.. include:: modules.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
