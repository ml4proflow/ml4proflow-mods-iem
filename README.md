# ml4proflow - A data flow oriented framework for ml applications in industry

A module for ml4proflow to perform simulation and analysis of a hot forming line
 
[![Tests Status](https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-mods-iem/-/jobs/artifacts/master/raw/tests-badge.svg?job=gen-cov)](https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-mods-iem/-/jobs/artifacts/master/file/reports/junit/report.html?job=gen-cov)
[![Coverage Status](https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-mods-iem/-/jobs/artifacts/master/raw/coverage-badge.svg?job=gen-cov)](https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-mods-iem/-/jobs/artifacts/master/file/reports/coverage/index.html?job=gen-cov)
[![Flake8 Status](https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-mods-iem/-/jobs/artifacts/master/raw/flake8-badge.svg?job=gen-cov)](https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-mods-iem/-/jobs/artifacts/master/file/reports/flake8/index.html?job=gen-cov)
[![mypy errors](https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-mods-iem/-/jobs/artifacts/master/raw/mypy.svg?job=gen-cov)]()
[![mypy strict errors](https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-mods-iem/-/jobs/artifacts/master/raw/mypy_strict.svg?job=gen-cov)]()
------------


# Usage 
To configure the module correctly, add the following module configuration to the configuration file: 
```json 
{
    "Module_batch" : 3,
    "Module_Index of batch" : 2
}
```

# Prerequisites
- [ml4proflow](https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow)

# Installation
Activate your virtual environment, clone this repository and install the package with pip:
```console 
$ pip install .
```

# Contribution
```console 
$ pip install -e .
```
