import unittest
from ml4proflow import modules
from ml4proflow_mods.iem.modules import Warmformen_IEM


class TestModulesModule(unittest.TestCase):
    def setUp(self):
        self.dfm = modules.DataFlowManager()

    def test_create_sink_Warmformen_IEM(self):
        dut = Warmformen_IEM(self.dfm, {})
        self.assertIsInstance(dut, Warmformen_IEM)


if __name__ == '__main__':  # pragma: no cover
    unittest.main()
